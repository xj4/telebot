package main

import (
	yaml "gopkg.in/yaml.v2"
	"io/ioutil"
)

type PG struct {
	Server ServConfig `yaml:"engine,inline"`
	Log    *Logger    `yaml:"log"`
}

type ServConfig struct {
	Port   string `yaml:"port"`
	ApiUrl string `yaml:"apiurl"`
	Token  string `yaml:"token"`
}

type TelegramBot struct {
	UpdateID int `json:"update_id"`
	Message  struct {
		MessageID int `json:"message_id"`
		From      struct {
			ID           int    `json:"id"`
			IsBot        bool   `json:"is_bot"`
			FirstName    string `json:"first_name"`
			LastName     string `json:"last_name"`
			LanguageCode string `json:"language_code"`
		} `json:"from"`
		Chat struct {
			ID        int    `json:"id"`
			FirstName string `json:"first_name"`
			LastName  string `json:"last_name"`
			Type      string `json:"type"`
		} `json:"chat"`
		Date int    `json:"date"`
		Text string `json:"text"`
	} `json:"message"`
}

type IpAPI struct {
	Status      string  `json:"status"`
	Country     string  `json:"country"`
	CountryCode string  `json:"countryCode"`
	Region      string  `json:"region"`
	RegionName  string  `json:"regionName"`
	City        string  `json:"city"`
	Zip         string  `json:"zip"`
	Lat         float64 `json:"lat"`
	Lon         float64 `json:"lon"`
	Timezone    string  `json:"timezone"`
	Isp         string  `json:"isp"`
	Org         string  `json:"org"`
	As          string  `json:"as"`
	Query       string  `json:"query"`
}

type WhoApi struct {
	Domain string `json:"domain"`
	Error  string `json:"error"`
	Data   struct {
		Available       bool        `json:"available"`
		Expiration      string      `json:"expiration"`
		Creation        string      `json:"creation"`
		RegistrantEmail interface{} `json:"registrant_email"`
		RegistrantName  interface{} `json:"registrant_name"`
	} `json:"data"`
}

func NewCfg(config string) PG {
	buf, err := ioutil.ReadFile(config)
	if err != nil {
		panic(err)
	}
	var d PG
	err = yaml.Unmarshal(buf, &d)
	if err != nil {
		panic(err)
	}
	return d
}
func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
