package main

import (
    "encoding/json"
    "fmt"
    "net/http"

    "bytes"
    // / "io/ioutil"
    "regexp"
    "strings"
    //"log"
    //"strconv"
    "io/ioutil"
)

var r1 = regexp.MustCompile(`^([f|F]uck)$|^([h|H]elp)$|^(\?)$|^(f*ck)$`)
var r2 = regexp.MustCompile(`^[w|W]ho\s{1,100}(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])\s{0,100}$`)
var r3 = regexp.MustCompile(`^[i|I]p\s{0,10}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$`)

func ClientIP(r *http.Request) string {
    clientIP := strings.TrimSpace(r.Header.Get("X-Real-Ip"))
    if len(clientIP) > 0 {
        return clientIP
    }
    clientIP = r.Header.Get("X-Forwarded-For")
    if index := strings.IndexByte(clientIP, ','); index >= 0 {
        clientIP = clientIP[0:index]
    }
    clientIP = strings.TrimSpace(clientIP)
    if len(clientIP) > 0 {
        return clientIP
    }
    if r.RemoteAddr[0] == '[' { // IPv6 Address
        if index := strings.IndexByte(r.RemoteAddr, ']'); index >= 0 {
            clientIP = r.RemoteAddr[1:index]
        }
        return clientIP
    }

    if index := strings.IndexByte(r.RemoteAddr, ':'); index >= 0 {
        clientIP = r.RemoteAddr[0:index]
    }
    return strings.TrimSpace(clientIP)
}

func Whitelist(ip string) bool {
    var whitelist = []string{
        "10.10.10.1",
        "10.10.10.2",
    }
    var result bool
    for _, ipw := range whitelist {
        // Check ip in whitelist
        if ipw == ip {
            result = true // If have
            break
        } else {
            result = false
        }
    }
    return result
}

func Load() PG {
    cfg := NewCfg("bot.yaml")
    return cfg
}

func MessHandler(cfg PG, uid string, msg string) int {
    var flag = 2018
    switch {
    case r1.MatchString(msg):
        // Case help
        flag = 1
        break
    case r2.MatchString(msg):
        // Case Grafana
        flag = 2
        break
    case r3.MatchString(msg):
        // Case help
        //fmt.Println(msg)
        flag = 3
        break
    default:
        flag = 1000
    }

    return flag
}

func SendMessage(cfg PG, chat_id string, msg string) {

    //log.Println("msg:", msg)
    url := fmt.Sprintf(cfg.Server.ApiUrl, cfg.Server.Token)
    data := `{"chat_id":"` + chat_id + `","text":"` + msg + `""}`
    var jsonStr = []byte(data)
    req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
    req.Header.Set("X-Custom-Header", "myvalue")
    req.Header.Set("Content-Type", "application/json")

    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        panic(err)
    }
    defer resp.Body.Close()
    /*
       fmt.Println("response Status:", resp.Status)
       fmt.Println("response Headers:", resp.Header)
       body, _ := ioutil.ReadAll(resp.Body)
       fmt.Println("response Body:", string(body))

       fmt.Println("msg:", msg)
    */
}

func GetAPI(hmsg string) string {
    words := strings.Fields(hmsg)
    resp, err := http.Get("http://ip-api.com/json/" + words[1])
    if err != nil {
        fmt.Println(err)
    }

    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)

    var i IpAPI
    err = json.Unmarshal(body, &i)
    if err != nil {
        panic(err)
    }

    if err != nil {
        // handle error
    }
    msg := "- Country: " + i.Country + "\n- CountryCode: " + i.CountryCode + "\n- RegionName: " + i.RegionName + "\n- City: " + i.City + "\n- ISP: " + i.Isp + "\n- AS: " + i.As
    return msg

}

func WhoisGet(hmsg string) string {
    var msg string
    words := strings.Fields(hmsg)
    domain := words[1]
    resp, err := http.Get("https://madchecker.com/domain/api/" + domain + "?properties=expiration-creation-registrant_email-registrant_name-registrant_email")
    if err != nil {
        fmt.Println(err)
    }

    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)

    var i WhoApi
    err = json.Unmarshal(body, &i)
    if err != nil {
        panic(err)
    }

    if err != nil {
        // handle error
    }
    if len(i.Error) > 0 {
        msg = "- Domain: " + i.Domain + "\n- Error: " + i.Error
    } else {
        msg = "- Domain: " + i.Domain + "\n- Available: " + fmt.Sprintf("%v", i.Data.Available) + "\n- Expiration: " + i.Data.Expiration
    }
    return msg

}
