package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	//"time"
)

var welcommsg = `Hi %s %s
Welcome My bot.
 Use: 
 - ip [IP] : Get server information search by IP(Public IP).
 - who [DOMAIN] : Get domain information - Whois Lookup.

 `

var func404 = `Hi %s %s, function  \" %s \" not found or wrong format  👻 . You can use: \"help\" for get more infomation about BOT. Thank you.`

func handler(res http.ResponseWriter, req *http.Request, cfg PG) {

	if req.Method != "POST" {
		fmt.Fprint(res, string("Method not allow ! "))
		return
	}

	// Debug, print body
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		panic(err)
	}
	//fmt.Println(string(body))
	var t TelegramBot
	err = json.Unmarshal(body, &t)
	if err != nil {
		panic(err)
	}
	message := t.Message.Text
	msgflag := MessHandler(cfg, "", message)
	fmt.Println("Flag: ", msgflag)
	switch msgflag {
	case 1:
		msg := fmt.Sprintf(welcommsg, t.Message.From.LastName, t.Message.From.FirstName)
		SendMessage(cfg, strconv.Itoa(t.Message.From.ID), msg)

	case 2:
		SendMessage(cfg, strconv.Itoa(t.Message.From.ID), WhoisGet(message))

	// Show Ip info HANDLE
	case 3:
		//fmt.Println("Check IP Case")
		SendMessage(cfg, strconv.Itoa(t.Message.From.ID), GetAPI(message))

	case 1000:
		SendMessage(cfg, strconv.Itoa(t.Message.From.ID), fmt.Sprintf(func404, t.Message.From.LastName, t.Message.From.FirstName, message))
	}

}
