package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log/syslog"
	"net/http"
	"runtime"
	"time"

	"github.com/sirupsen/logrus"
	logrus_syslog "github.com/Sirupsen/logrus/hooks/syslog"
	"gopkg.in/natefinch/lumberjack.v2"
)

type ErrorLog struct {
	Out            *lumberjack.Logger
	disable        bool
	*logrus.Logger `json:"-"`
}

type Logger struct {
	ErrorLog       string            `yaml:"errorlog"`
	Level          string            `yaml:"level"`
	Formatter      string            `yaml:"format"`
	Syslog         string            `yaml:"syslog"`
	FileLog        string            `yaml:"logfile"`
	FileLogger     lumberjack.Logger `yaml:",inline"`
	ErrLog         *ErrorLog
	*logrus.Logger `json:"-"`
}

var log *Logger

func (l *Logger) Errorln(args ...interface{}) {
	if l.ErrLog.disable == true {
		return
	}
	l.ErrLog.Errorln(args)
}

func (l *Logger) Errorf(format string, args ...interface{}) {
	if l.ErrLog.disable == true {
		return
	}
	l.ErrLog.Errorf(format, args)
}

func (l *Logger) Init() {
	if l.Logger == nil {
		l.Logger = logrus.New()
	}

	var writer io.Writer
	if l.FileLogger.Filename == "-" || l.FileLogger.Filename == "" {
		writer = ioutil.Discard
	} else {
		writer = &l.FileLogger
		if l.FileLogger.MaxAge == 0 {
			l.FileLogger.MaxAge = 28
		}
		if l.FileLogger.MaxBackups == 0 {
			l.FileLogger.MaxBackups = 10
		}
		if l.FileLogger.MaxSize == 0 {
			l.FileLogger.MaxSize = 20
		}
	}

	l.Logger.Out = writer

	switch l.Formatter {
	case "text":
		l.Logger.Formatter = &logrus.TextFormatter{}
	case "json":
		l.Logger.Formatter = &logrus.JSONFormatter{}
	default:
		l.Logger.Formatter = &logrus.JSONFormatter{}
	}

	switch l.Level {
	case "info":
		l.Logger.Level = logrus.InfoLevel
	case "debug":
		l.Logger.Level = logrus.DebugLevel
	case "error":
		l.Logger.Level = logrus.ErrorLevel
	case "off":
		l.Logger.Level = logrus.FatalLevel
	default:
		l.Logger.Level = logrus.ErrorLevel
	}

	var protocol, host string

	if len(l.Syslog) > 6 {
		switch l.Syslog[0:6] {
		case "udp://":
			protocol = "udp"
			host = l.Syslog[6:]
		case "tcp://":
			protocol = "tcp"
			host = l.Syslog[6:]
		case "unix:/":
			protocol = "unix"
			host = l.Syslog[7:]
		}
	}

	if len(protocol) > 0 {
		shook, err := logrus_syslog.NewSyslogHook(protocol, host, syslog.LOG_INFO, "")
		if err != nil {
			log.Error("Unable to connect to local syslog daemon")
		} else {
			l.Logger.Hooks.Add(shook)
		}
	}

	if l.ErrLog == nil {
		l.ErrLog = &ErrorLog{
			disable: false,
			Logger:  logrus.New(),
			Out: &lumberjack.Logger{
				MaxAge:     l.FileLogger.MaxAge,
				MaxSize:    l.FileLogger.MaxSize,
				MaxBackups: l.FileLogger.MaxBackups,
			},
		}
		l.ErrLog.Logger.Out = l.ErrLog.Out
		l.ErrLog.Logger.Formatter = &logrus.TextFormatter{}
	}

	if l.ErrorLog == "-" {
		l.ErrLog.disable = true
	} else {
		l.ErrLog.Out.Filename = l.ErrorLog
	}

	log = l
}

func (l *Logger) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	start := time.Now()
	next(rw, r)
	result := rw.Header().Get("STATS")
	postForm := ""
	for k, v := range r.PostForm {
		postForm += fmt.Sprintf("%s=%v", k, v)
	}
	l.WithFields(logrus.Fields{
		"method":       r.Method,
		"uri":          r.RequestURI,
		"req":          postForm,
		"process_time": time.Since(start),
		"client":       r.RemoteAddr,
		"host":         r.Host,
		"real_ip":      ClientIP(r),
		"result":       result,
	}).Info()
}

// Recovery is a Negroni middleware that recovers from any panics and writes a 500 if there was one.
type Recovery struct {
	Logger     *Logger
	PrintStack bool
	StackAll   bool
	StackSize  int
}

// NewRecovery returns a new instance of Recovery
func NewRecovery(cfg *PG) *Recovery {
	return &Recovery{
		Logger:     cfg.Log,
		PrintStack: true,
		StackAll:   false,
		StackSize:  1024 * 8,
	}
}

func (rec *Recovery) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	defer func() {
		if err := recover(); err != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			stack := make([]byte, rec.StackSize)
			stack = stack[:runtime.Stack(stack, rec.StackAll)]

			f := "PANIC: %s\n%s"
			rec.Logger.Errorf(f, err, stack)

			if rec.PrintStack {
				fmt.Fprintf(rw, f, err, stack)
			}
		}
	}()

	next(rw, r)
}
