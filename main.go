package main

import (
    "github.com/gorilla/mux"
    "net/http"
)

var envflag int
var ServePort string

func main() {
    // Init load

    cfg := NewCfg("bot.yaml")
    if cfg.Log == nil {
        cfg.Log = &Logger{}
    }
    cfg.Log.Init()
    router := mux.NewRouter()
    ServePort = cfg.Server.Port
    router.HandleFunc("/webhook", func(w http.ResponseWriter, r *http.Request) { handler(w, r, cfg) })
    http.ListenAndServe(ServePort, router)

}
